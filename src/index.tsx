import './styles/main.scss';

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { HashRouter, withRouter } from 'react-router-dom';
import { Provider } from 'mobx-react';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import mainStore from "./store/MainStore";


import Main from './modules/Main';
import muiThemeStyles from './muiThemeStyles';

const stores = {
    ...mainStore,
};

const muiTheme = createMuiTheme(muiThemeStyles as any);

const MainWithRouter = withRouter(Main);

ReactDOM.render(
    <Provider {...stores} >
        <HashRouter>
            <MuiThemeProvider theme={muiTheme}>
                <MainWithRouter />
            </MuiThemeProvider>
        </HashRouter>
    </Provider>,
    document.getElementById('main')
);
