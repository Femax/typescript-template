import { FONT_SIZE, PRIMARY_COLOR, PRIMARY_LIGHT_COLOR, PRIMARY_DARK_COLOR, SECONDARY_COLOR, SECONDARY_LIGHT_COLOR, SECONDARY_DARK_COLOR } from './constants';

export default {
    // breakpoints: {
    //     values: ['md'],
    // },
    palette: {
        primary: {
            main: PRIMARY_COLOR,
            light: PRIMARY_LIGHT_COLOR,
            dark: PRIMARY_DARK_COLOR,
            contrastText: '#FFF',
        },
        secondary: {
            main: SECONDARY_COLOR,
            light: SECONDARY_LIGHT_COLOR,
            dark: SECONDARY_DARK_COLOR,
            contrastText: '#000',
        },
        // action: {
        //     selected: 'rgba(255, 175, 78, 1)'
        // }
    },
    typography: {
        fontFamily: ['Roboto', 'sans-serif'],
        fontSize: FONT_SIZE,
        h5: {
            fontSize: 22,
        },
        h6: {
            fontSize: 19,
        },
        subtitle1: {
            fontSize: 15,
        },
        subtitle2: {
            fontSize: 13,
        },
        body1: {
            fontSize: FONT_SIZE,
        },
        useNextVariants: true,
    },
    overrides: {
        MuiSvgIcon: {
            root: {
                fontSize: 18,
            },
        },
        MuiButton: {
            root: {
                textTransform: 'none',
            },
        },
        MuiTabs: {
            root: {
                marginLeft: 12,
                marginRight: 12,
            },
            indicator: {
                height: 3,
                // backgroundColor: PRIMARY_DARK_COLOR,
            },
        },
        MuiTab: {
            root: {
                textTransform: 'none',
                marginLeft: 12,
                marginRight: 12,
                minWidth: 20,
                '@media (min-width: 960px)': {
                    minWidth: 20,
                },
            },
            label: {
                fontSize: 16,
                fontWeight: 800,
            },
            labelContainer: {
                paddingLeft: 0,
                paddingRight: 0,
                '@media (min-width: 960px)': {
                    paddingLeft: 0,
                    paddingRight: 0,
                },
            },
            textColorInherit: {
                color: '#000000',
                opacity: 0.4,
            },
        },
        MuiListItem: {
            root: {
                paddingTop: 4,
                paddingBottom: 4,
            },
        },
        MuiTableRow: {
            root: {
                height: 32,
            },
            head: {
                height: 32,
            },
        },
        MuiTableCell: {
            root: {
                padding: '0 important!',
            },
            head: {
                fontSize: FONT_SIZE,
            },
            body: {
                fontSize: FONT_SIZE,
            },
        },
        MuiIconButton: {
            root: {
                height: 32,
                width: 32,
                padding: 0,
            },
        },
        MuiFab: {
            root: {
                height: 32,
                width: 36,
                backgroundColor: '#ffffff',
            },
        },
        MuiSelect: {
            selectMenu: {
                minHeight: '0.1700em',
            },
        },
        // MuiSwitch: {
        //     root: {
        //         width: 32
        //     }
        // },
    },
    zIndex: {
        mobileStepper: 10,
        appBar: 11,
        drawer: 12,
        modal: 13,
        snackbar: 14,
        tooltip: 15,
    },
};
