import SearchStore from "../modules/Search/SearchStore";

class MainStore {
    searchStore: SearchStore
    constructor() {
        this.searchStore = new SearchStore();
    }
}

export default new MainStore()