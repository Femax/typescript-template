import { observable, computed, action } from 'mobx';

class SearchStore {

    @observable
    text: string = "asd"
    

    @computed get getText() {
        return this.text
    }

    @action onChangeText = (element: any) => {
        this.text = element.target.value
    }

}

export default SearchStore;