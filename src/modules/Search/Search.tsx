import * as React from "react"
import SearchStore from "./SearchStore";
import { TextField, Theme, InputAdornment } from "@material-ui/core";
import { observer, inject } from "mobx-react";
import { fade, withStyles, createStyles, createMuiTheme, makeStyles } from '@material-ui/core/styles';
import Visibility from '@material-ui/icons/Visibility';
import "./Search.scss"

interface Props {
    searchStore: SearchStore
}
const useStylesReddit = makeStyles(theme => ({
    root: {
        border: '1px solid #e2e2e1',
        overflow: 'hidden',
        borderRadius: 4,
        backgroundColor: '#fcfcfb',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        '&:hover': {
            backgroundColor: '#fff',
        },
        '&$focused': {
            backgroundColor: '#fff',
            boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 2px`,
            borderColor: theme.palette.primary.main,
        },
    },
    focused: {},
}));

function RedditTextField(props: any) {
    const classes = useStylesReddit(props.theme);

    return <TextField InputProps={{ classes, disableUnderline: true }} {...props} />;
}



const block = "search"
const Search = (props: Props) => {
    console.log(props)
    return (<div className={block + "__container"}>

        <div className={block + "__search-field"}>
            <p>Введите адресс</p>
            <RedditTextField
                label="Поиск"
                variant="filled"
                value={props.searchStore.getText}
                onChange={props.searchStore.onChangeText}
                // InputProps={{
                //     startAdornment: <InputAdornment position="start">
                //         <Visibility />
                //     </InputAdornment>,
                // }}
            />
        </div>
    </div>)

}

export default inject("searchStore")(observer(Search))