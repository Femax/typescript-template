import './Main.scss';

import React, { Component } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import SearchStore from '../Search/SearchStore';
import Search from '../Search/Search';

interface AtmsWindow extends Window {
    mapWin: any;
}

interface Props extends RouteComponentProps { }

interface State {
    loading: boolean;
    error: boolean;
    versionAppDialogOpen: boolean;
    loadingPlanning: boolean;
}

class Main extends Component<Props, State> {
    errorDialog: any;
    state = {
        loading: true,
        error: false,
        versionAppDialogOpen: false,
        loadingPlanning: false,
    };
    window: AtmsWindow;

    constructor(props: Props) {
        super(props);

        this.window = window as any;
        this.window.mapWin = null;
    }

    componentDidMount() {
    }



    render() {
        const { loading, error, versionAppDialogOpen } = this.state;
        return (
            <>
                <Search />
            </>
        );
    }
}
export default Main