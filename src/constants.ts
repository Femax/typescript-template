export const FONT_SIZE = 14;

// colors
export const PRIMARY_COLOR = '#EF7F1A';
export const PRIMARY_LIGHT_COLOR = '#FFAF4E';
export const PRIMARY_DARK_COLOR = '#B65100';

export const SECONDARY_COLOR = '#EF7F1A';
export const SECONDARY_LIGHT_COLOR = '#FFAF4E';
export const SECONDARY_DARK_COLOR = '#B65100';

export const SELECTED_ITEM_BACKGROUND_COLOR = 'rgba(239, 127, 26, 0.25)';
