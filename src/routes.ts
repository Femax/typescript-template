import Main from "./modules/Main";


export interface Routes {
    key: string;
    path: string;
    component: any;
    name?: string;
    visible?: boolean;
}

const routes: Routes[] = [
    { key: 'control', path: '/control', name: 'Планирование и Диспетчеризация', component: Main }, 
];

export default routes;
