const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackBuildNotifierPlugin = require('webpack-build-notifier');
const IgnorePlugin = webpack.IgnorePlugin;
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const FilterWarningsPlugin = require('webpack-filter-warnings-plugin');

var packageJson = require('./package.json');

module.exports = (env, argv) => {
    const prod = argv.mode === 'production';

    return {
        devtool: prod ? 'source-map' : 'eval-source-map',
        stats: {
            colors: true
        },
        devServer: {
            contentBase: path.join(__dirname, 'dist'),
            compress: true,
            port: 9000
          },
        entry: {
            app: ['@babel/polyfill', './src/index.tsx']
        },
        output: {
            publicPath: '/',
            filename: '[name].min.js',
            path: path.resolve(__dirname, 'build')
        },
        optimization: {
            splitChunks: {
                cacheGroups: {
                    commons: {
                        // test: /[\\/]node_modules[\\/]/,
                        test: /[\\/]node_modules[\\/](?!utlexport)(.[a-zA-Z0-9.\-_]+)[\\/]/,
                        name: 'vendor',
                        chunks: 'all'
                    }
                }
            }
        },
        resolve: {
            extensions: ['.ts', '.tsx', '.js', '.json']
        },
        module: {
            rules: [
                {
                    test: /.(js|jsx|ts|tsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader'
                    }
                },
                {
                    test: /.s?css$/,
                    use: [
                        // creates style nodes from JS strings
                        prod ? MiniCssExtractPlugin.loader : 'style-loader',
                        // translates CSS into CommonJS
                        'css-loader',
                        // compiles Sass to CSS, using Node Sass by defaul
                        'sass-loader'
                    ]
                },
                {
                    test: /\.(png|jpg|gif|svg)$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                outputPath: 'img/'
                            }
                        }
                    ]
                },
                {
                    test: /\.(eot|eot|otf|ttf|woff|woff2)$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                outputPath: 'font/'
                            }
                        }
                    ]
                },
            ]
        },
        plugins: [
            new FilterWarningsPlugin({
                exclude: /\[mini-css-extract-plugin\][^]*Conflicting order between:/,
            }),
            new MiniCssExtractPlugin({
                filename: '[name].css',
                chunkFilename: '[id].css',
            }),
            new HtmlWebpackPlugin({
                hash: true,
                title: 'Mega otzivayzer',
                template: 'src/index.html',
                chunks: ['vendor', 'app'],
                favicon: 'src/favicon.ico',
            }),
            new IgnorePlugin({
                resourceRegExp: /^\.\/locale$/,
                contextRegExp: /moment$/
            }),
            // при необходимости раскоментить
            // new BundleAnalyzerPlugin({
            //     analyzerMode: 'static',
            //     reportFilename: 'bundle-analyzer-report.html',
            //     openAnalyzer: false,
            // }),
        ]
    };
};
